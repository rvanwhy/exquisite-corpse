#pragma once
#include "game.hpp"

class Agent
{
public:
    bool alive;

    float money;
    float hfactor;

    int elo;
    float win, loss;

    int x, y;
    int id;

    Agent *mind; //The agent on this agent's mind (WTF?)

    Agent();
    void draw();

    // color suggestion for this agent
    float color();

    // does this agent overlap another?
    bool overlaps(Agent& other);

    // move
    void random_move();

    void grab_money(Agent& other);
};

// show info
std::ostream& operator<<(std::ostream& out, Agent& a);
