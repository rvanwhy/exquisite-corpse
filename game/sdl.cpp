#include "game.hpp"

void init(void)
{
    SDL_Surface *screen = NULL;
    SDL_Init(SDL_INIT_EVERYTHING);
    screen = SDL_SetVideoMode(SCREEN_WIDTH, SCREEN_HEIGHT,
                              SCREEN_DEPTH, SDL_OPENGL);
    SDL_WM_SetCaption("Moneygrab", NULL);
}

void init_opengl(void)
{
    //Initialize Projection Matrix
    glMatrixMode( GL_PROJECTION );
    glLoadIdentity();
    glOrtho( 0.0, SCREEN_WIDTH, SCREEN_HEIGHT, 0.0, 1.0, -1.0 );

    //Initialize Modelview Matrix
    glMatrixMode( GL_MODELVIEW );
    glLoadIdentity();

    //Initialize clear color
    glClearColor( .25f, 0.5f, 0.75f, 1.f );
}

void renderAgents(Agent *agents)
{
    glClear(GL_COLOR_BUFFER_BIT );
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glLoadIdentity();

    for(int i = 0; i < POPULATION; i++)
        agents[i].draw();

    SDL_GL_SwapBuffers();
}

bool onTopOf(Agent first, Agent second)
{
    return distanceBetween(first, second) == 0;
}

float distanceBetween(Agent first, Agent second)
{
    return sqrt(pow(first.x - second.x, 2) + pow(first.y - second.y, 2));
}

/**
 * In the world of moneygrab, the radius of speech is two agent units
 * This translates to three units as placement in space is determined by
 * the lower left corner of the agent.
 */
bool withinSpeechRadius(Agent first, Agent second)
{
    return distanceBetween(first, second) < SIZE*3;
}
