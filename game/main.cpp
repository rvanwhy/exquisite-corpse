/**
* Moneygrab
* An experimental simulation
* Randall Van Why
* 01-06-2013
*/
#include "game.hpp"
#include <algorithm>
#include <iterator>

using namespace std;

/*
  The ELO Rating system will be used in this simulation.
  Elo is based on wins/losses of monetary exchanges during
  the simulation's run. Though all agents start at the same elo,
  the adjustments should skyrocket some to high elo and shoot some
  to lower elo. Those at extrema of ELO, when faced with extreme
  wins/losses are more likely to commit crimes to win (or not lose).

  hFactor the agent's likelyhood to commit crime. Tied with elo
  (one must take into account that crime may give one bad rep) which
  would negatively affect h. Magnitudes of crime must be taken into account
*/

enum {MONDAY, TUESDAY, WEDNESDAY, THURSDAY, FRIDAY};

void seed(Agent *agents)
{
    srand(time(NULL));
    for (int i = 0; i < POPULATION; i++)
        agents[i].id = i;
}

void print_info(Agent *agents)
{
    ostream_iterator<Agent&> out(cout, "\n");
    copy(&agents[0], &agents[POPULATION], out);
}

void translate_agents(Agent *agents)
{
    for_each(agents, &agents[POPULATION],
             mem_fn(&Agent::random_move));
}

void evaluate_hfactor_changes(Agent **agents)
{
    //Blank for now, while we rework this
}

void print_csv(Agent *agents)
{
    FILE *fp;
    fp = fopen("out.csv", "w");
    fprintf(fp, "id,alive,elo,money,hfactor,rep\n");
    for (int i = 0; i < POPULATION; i++) {
        fprintf(fp, "%d,%s,%d,%f,%f,%f\n", agents[i].id,
                agents[i].alive ? "True" : "False",
                agents[i].elo,
                agents[i].money,
                agents[i].hfactor,
                0.0/*agents[i].reputationBank[i]*/);
    }
}

int main(void)
{
    Agent* agents = new Agent[POPULATION];
    int day;
    bool done = false;
    SDL_Event event;
    seed(agents);
    init();
    init_opengl();

    while(!done) {
        while(SDL_PollEvent(&event)) {
            if(event.type == SDL_QUIT)
                done = true;
        }

        if(SDL_GetTicks() % 250 == 0) {
            day++;
            translate_agents(agents);
            for (int i = 0; i < POPULATION; i++) {
                for (int j = i+1; j < POPULATION; j++) {
                    if (agents[i].overlaps(agents[j]))
                        agents[i].grab_money(agents[j]);
                }
            }

            //evaluate_hfactor_changes(&agents);
            renderAgents(agents);
        }
    }

    SDL_Quit();
    cout << "The games have concluded." << day <<" days have passed...\n";
    cout << "Here are the results:" << endl;
    print_info(agents);
    print_csv(agents);
    delete agents;
    return 0;
}
