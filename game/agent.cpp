#include <iomanip>
#include "game.hpp"

using namespace std;

Agent::Agent()
{
    //reputationBank = new float[POPULATION]();
    alive = true;
    elo = 1200;
    hfactor = elo / 1200;
    money = (float)TOTAL_MONEY / POPULATION;
    x = rand() % HEIGHT + 1;
    y = rand() % WIDTH + 1;
    win = 0.0;
    loss = 0.0;
}

float Agent::color()
{
    return this->money / TOTAL_MONEY * 10;
}

void Agent::draw()
{
    //Agent size is calculated here
    float pixelw = (float) SCREEN_WIDTH / (float) WIDTH;
    float pixelh = (float) SCREEN_HEIGHT / (float) HEIGHT;

    glBegin( GL_QUADS );
    glColor3f(this->color(),this->color(), this->color());
    glVertex2f(pixelw*this->x,       pixelh*this->y);
    glVertex2f(pixelw*(this->x + 1), pixelh*this->y);
    glVertex2f(pixelw*(this->x + 1), pixelh*(this->y + 1));
    glVertex2f(pixelw*this->x,       pixelh*(this->y + 1));
    glEnd();

}

bool Agent::overlaps(Agent& other)
{
    return x == other.x && y == other.y;
}

ostream& operator<<(ostream& out, Agent& a)
{
    out << "Agent " << a.id << endl;
    out << "This agent is " << (a.alive ? "alive" : "dead") << endl;
    out << "Elo: "<< a.elo << endl;
    out << "hFactor: " << fixed << setprecision(1) << a.hfactor << endl;
    out << "Money: $"  << fixed << setprecision(2) << a.money   << endl;
    // printf("Reputation: %.2f\n",/*agents[i].reputationBank[i]*/ 0.0);
    return out;
}

void Agent::random_move()
{
    x = (WIDTH + (x + (rand() % 3) - 1)) % WIDTH;
    y = (HEIGHT + (y + (rand() % 3) - 1)) % HEIGHT;
}

//This is a BIG misapplication of ELO
void Agent::grab_money(Agent &other)
{
    float win1, win2;
    float aelo1, aelo2;
    float loss;

    loss = ((money > other.money) ? other.money : money) / 2.0;

    win1 = 1/(1 + pow(10, (aelo1 - aelo2) / 400.0));
    win2 = 1/(1 + pow(10, ((elo - other.elo) / 400.0)));

    if (determine_winner(win1)) {
        money += loss;
        other.money -= loss;
        win += 1;
        other.loss += 1;
    } else {
        other.money += loss;
        money -= loss;
        other.win += 1;
        loss += 1;
    }

    elo += K*(win / (win + loss) - win2);
    other.elo += K*(other.win / (other.win + other.loss) - 1 - win2);
}

bool determine_winner(float probability)
{
    return (float)rand() / (float)RAND_MAX < probability;
}
