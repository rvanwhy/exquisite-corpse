#pragma once

#include <iostream>
#include <vector>
#include <math.h>

#include "SDL/SDL.h"
#include "SDL/SDL_opengl.h"

#include "agent.hpp"

/*OpenGl Stuff*/
void init(void);
void init_opengl(void);
void display(void);
void renderAgents(Agent *agents);
bool onTopOf(Agent first, Agent second);
float distanceBetween(Agent first, Agent second);
bool withinSpeechRadius(Agent first, Agent second);


void seed(Agent agents[]);
void print_info(Agent agents[]);
void moneygrab(Agent *agent1, Agent *agent2);
void translate_agents(Agent *agents[]);
bool determine_winner(float probability);
bool compare_agents(Agent agent1, Agent agent2);
void evaluate_hfactor_changes(Agent **agents);
void print_csv(Agent *agents);
const int K = 32;


// gl Defines
const int SCREEN_WIDTH  = 600;
const int SCREEN_HEIGHT = 600;
const int SCREEN_DEPTH  =  32;
const int PIXEL_HEIGHT  =  50;
const int PIXEL_WIDTH   =  50;
const int FPS           =  60;

// game defines
const int SIZE          =  30;
const int WIDTH         =  30;
const int HEIGHT        =  30;
// area = 900units
const int TOTAL_MONEY   =  1000000;
const int POPULATION    =  50;
