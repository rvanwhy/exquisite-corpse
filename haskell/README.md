MoneyGrab
=========

Hello Functional People!

I finally taught myself Haskell, and to celebrate I used it to
re-write this exquisite corpse.

## Requirements

Do this:

    cabal install random SDL

and you should be good to go.

## The Game

Here's what's going on what with all the squares and the colors:

First a population of "Agents" is established. Each agent is placed at
a random coordinate on the grid, and given their share of some fixed
amount of money.

Each agent takes a single step in a random direction. If two (or more)
agents overlap each other they compete, and the loser gives some money
to the winner (probability favors the agent with the more money).

The loser might die if they drop below some threshold of money. If
they do die, any agent that lands on them will take the rest of the
money from their corpse.

If they survive, they will have their opponent stuck on their mind, as
if in search of revenge. When an agent has another agent on their
mind, they stop taking random steps and start moving toward the agent
they're hunting.

Finally, after each exchange of money, the agent with the most money
begins hunting the agent with the second greatest amount of money, and
the second richest agent begins hunting the living agent with the
least money.

The shade of each square indicates how much money that agent has:
brighter is richer, darker is poorer.

The green squares represent two or more agents who are exchanging money.

The red squares are actively hunting other agents.

Those black squares, the ones that aren't moving, those are dead
agents.

## TODO

This has nothing to do with ELO right now. I should figure out what
that is and then add it to this mean economic system...

This is my first project in Haskell, so I'm sure the code is
shitty. I'll go over it again one day and do it better.