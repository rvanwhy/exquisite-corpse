module Agent where

import qualified System.Random as Rand
import Data.Function
import Data.List

data Agent = Agent {
      agentId :: Int
    , agentX :: Int
    , agentY :: Int
    , agentAlive :: Bool
    , agentMoney :: Money
    , agentWins :: [Agent]
    , agentLosses :: [Agent]
    , agentsOnMind :: [Agent]
    }

instance Show Agent where
    show a = let name = show $ agentId a
                 coord = show (agentX a, agentY a)
                 money = (show $ agentMoney a)
             in "{" ++ name ++ " : $" ++ money ++ " : " ++ coord ++ "}"

-- Agents are ordered based on their position, overlaps are equal
instance Ord Agent where
    compare a1 a2 = case (compare (agentY a1) (agentY a2)) of
                      EQ -> (compare (agentX a1) (agentX a2))
                      LT -> LT
                      GT -> GT

instance Eq Agent where
    a1 == a2 = (agentId a1) == (agentId a2)

type Money = Float
    

width :: Num a => a
height :: Num a => a
population :: Num a => a
totalMoney :: Money

width = 60
height = 60
population = 360
totalMoney = 50000000

evenMoney :: Money
evenMoney = totalMoney / population


newAgent :: Int -> IO Agent
newAgent num = do
  x <- Rand.randomRIO (0, width - 1) :: IO Int
  y <- Rand.randomRIO (0, height - 1) :: IO Int
  return $ Agent {
                 agentId = num
               , agentX = x
               , agentY = y
               , agentAlive = True
               , agentMoney = evenMoney
               , agentWins = []
               , agentLosses = []
               , agentsOnMind = []
               }


moveAgent :: Int -> Int -> Agent -> Agent
moveAgent dx dy a =
    if agentAlive a
    then a { agentX = (width + dx + agentX a) `mod` width
           , agentY = (height + dy + agentY a) `mod` height
           }
    else a

totalWins :: Agent -> Int
totalWins a = length $ agentWins a

              
totalLosses :: Agent -> Int
totalLosses a = length $ agentLosses a

                
killAgent :: Agent -> Agent
killAgent a = a {agentAlive = living }
  where living = 8 * (max 1 (totalWins a)) >= totalLosses a


setMoney :: Money -> Agent -> Agent
setMoney amt a = a { agentMoney = min totalMoney amt }

                 
wonLost :: (Agent, Agent) -> (Agent, Agent)
wonLost (w, l) =
    let won = w { agentWins = l : (agentWins w)
                , agentsOnMind = l `delete` (agentsOnMind w)
                }
        lost = l { agentLosses = w : (agentLosses l)
               -- Fool me once, shame on you.
               -- Fool me twice, I'll hunt you down and kill you.
                 , agentsOnMind = 
                     if agentMoney w <= agentMoney l && w `elem` agentLosses l
                     then won : agentsOnMind l
                     else agentsOnMind l
                 }
    in (won, lost)


pushOnMind :: Agent -> Agent -> Agent
pushOnMind na a = if agentAlive a
                  then a { agentsOnMind = na : agentsOnMind a }
                  else a


thinkingAbout :: Agent -> Agent -> Bool
a1 `thinkingAbout` a2 = a1 `elem` agentsOnMind a2
                        

seedAgents :: Int -> IO [Agent]
seedAgents p = do
  gen <- Rand.newStdGen
  as <- agentList pop
  return $ distributeMoney gen as
      where pop = abs p
            agentList 0 = return []
            agentList n = do
                   a <- newAgent (pop - n + 1)
                   as <- agentList (n - 1)
                   return $ a:as


moveAgents :: Rand.StdGen -> [Agent] -> [Agent]
moveAgents gen agents =
    let nums = Rand.randomRs (-1, 1) gen :: [Int]
        points = zip (drop population nums) nums
        moves = zipWith moveZip agents points
    in zipWith (uncurry moveAgent) moves agents
        where moveZip a (x, y) =
                  if null $ agentsOnMind a
                  then (x, y)
                  else let dist = 1 + x + y
                           target = head $ agentsOnMind a
                           (ax, ay) = (agentX a, agentY a)
                           (tx, ty) = (agentX target, agentY target)
                           trans v1 v2
                               | v1 > v2 = id
                               | v1 < v2 = negate
                               | otherwise = const 0
                       in if ax == tx && ay == ty
                          then (x * dist, y * dist)
                          else (trans tx ax $ dist, trans ty ay $ dist)


findOverlaps :: [Agent] -> [[Agent]]
findOverlaps as = filter ((> 1) . length) $ groupBy pos $ sort as
    where pos a b = compare a b == EQ


-- Add some slight variation to the amount of money each agent has
distributeMoney :: Rand.StdGen -> [Agent] -> [Agent]
distributeMoney _ [] = []
distributeMoney _ [a] = [a]
distributeMoney gen (a:as) = 
    let (r, ng) = Rand.randomR (-0.2, 0.2) gen :: (Money, Rand.StdGen)
        amnt = agentMoney a * (1 + r)
        dist = (agentMoney a - amnt) / (genericLength as)
    in (setMoney amnt a) : (distributeMoney ng $ map (addMoney dist) as)


addMoney :: Money -> Agent -> Agent
addMoney amt a = setMoney (agentMoney a + amt) a


exchangeMoney :: Agent -> Agent -> Money -> [Agent]
exchangeMoney winner loser amnt =
    let (wt, lt) = wonLost (winner, loser)
        w = addMoney amnt wt
        l = killAgent $ addMoney (-amnt) lt  -- I'm sure they survived...
    in [w, l]


-- The richest agent hunts the second richest agent
-- The second richest agent hunts the poorest agent
setHunters :: [Agent] -> [Agent]
setHunters agents =
    (capitalism richPeople) `union` agents
        where
          alive = filter agentAlive agents
          richPeople = sortBy (compare `on` (negate . agentMoney)) alive
                               
          capitalism [] = []
          capitalism [a] = [a]
          capitalism [a, b] = [pushOnMind b a, b]
          capitalism (a:as) =
            let poorest = last as
                second = pushOnMind poorest (head as)
                richest = pushOnMind second a
            in [richest, second]
            -- Blood-Bath Mode:
            -- in [richest, second] ++ capitalism (tail . init $ as)


youWin :: Rand.StdGen -> Agent -> Agent -> Bool
youWin gen a1 a2 = prob > rf
  where rf = (fst . Rand.random) gen :: Money
        (m1, m2) = (agentMoney a1, agentMoney a2)
        -- Bloodlust makes you stronger
        plus  = if a1 `thinkingAbout` a2 then 0.2 else 0
        minus = if a2 `thinkingAbout` a1 then 0.2 else 0
        prob  = (plus - minus) +  m1 / (m1 + m2)


-- Make all agents in a list compete for money!
grabMoney :: Rand.StdGen -> [Agent] -> [Agent]
grabMoney _ [] = []
grabMoney _ [a] = [a]
grabMoney gen [a1, a2] =
    let lives = map agentAlive [a1, a2]
    in case lives of
         [ True,  True] -> let amnt = (min (agentMoney a1) (agentMoney a2)) / 4
                           in if youWin gen a1 a2
                              then exchangeMoney a1 a2 amnt
                              else exchangeMoney a2 a1 amnt
         -- If one is dead, totally loot the corpse
         [True, False] -> exchangeMoney a1 a2 (agentMoney a2)
         [False,  True] -> exchangeMoney a2 a1 (agentMoney a1)
         -- If both are dead, do nothing
         [False, False] -> [a1, a2]
grabMoney gen (aa:ab:rest) =
    let g1 = (snd . Rand.next) gen
        (g2, g3) = Rand.split g1
        [a1, b1] = grabMoney g3 [aa, ab]
        [a,  c1] = grabMoney g2 [a1, head rest]
        [b,  c]  = grabMoney g1 [b1, c1]
        end = grabMoney gen $ tail rest
    in a:b:c:end


nextGen :: Rand.StdGen -> [Agent] -> [Agent]
nextGen gen agents =
  let ols = findOverlaps agents
  in if null ols
     then moveAgents gen agents
     else let fighters = concat $ map (grabMoney gen) ols
              returningAs = fighters `union` agents
              pissedOff = setHunters returningAs
              vagrants = moveAgents gen pissedOff
          in vagrants


printInfo :: Agent -> IO ()
printInfo agent = do
  putStrLn $ "Agent " ++ show (agentId agent) ++ ":"
  if agentAlive agent
  then putStrLn "\tStatus: Alive"
  else putStrLn "\tStatus: Dead"
  putStrLn $ "\tNet Worth: $" ++ show (agentMoney agent)
  putStr $ "\tRecord: " ++ show (length $ agentWins agent) ++ " Wins "
  putStrLn $ "/ " ++ show (length $ agentLosses agent) ++ " Losses"
