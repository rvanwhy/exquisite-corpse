import qualified Graphics.UI.SDL as SDL
import Data.Word
import Agent

import Data.List
import Data.Function

type RGB = (Word8, Word8, Word8)

scrW :: Int
scrW = 600
       
scrH :: Int
scrH = 600

eachW :: Int
eachW = scrW `div` width

eachH :: Int
eachH = scrH `div` height

        
clearSurface :: RGB -> SDL.Surface -> IO ()
clearSurface (r, g, b) screen = do
  let format = SDL.surfaceGetPixelFormat screen
  color <- SDL.mapRGB format r g b
  SDL.fillRect screen Nothing color
  return ()


agentRect :: Agent -> SDL.Rect
agentRect agent = let x = agentX agent * eachW
                      y = agentY agent * eachH
                  in SDL.Rect x y eachW eachH


-- Determines the color of an agent based on its status among other
-- agents
agentRGB :: [Agent] -> Agent -> RGB
agentRGB as agent =
    case agentAlive agent of
      True -> let ol = agent `elem` (concat $ findOverlaps as)
                  mind = not $ null (agentsOnMind agent)
                  shade = (+ 0x20) $ ceiling $ 0xC0 * percent
                      where percent = (agentMoney agent - lm) / (mm - lm)
                            minmax = (\(n, m) x -> (min x n, max x m))
                            (lm, mm) = foldl minmax (hm, hm) money
                            money = map agentMoney as
                            hm = head money
                  hshade = shade `div` 3
              in if ol
                 then (hshade, shade, hshade) -- Green overlaps
                 else if mind
                      then (shade, hshade, hshade) -- Red hunters
                      else (shade, shade, shade) -- Gray everyone else
      False -> (0, 0, 0) -- Black dead


drawAgents :: [Agent] -> SDL.Surface -> IO ()
drawAgents as screen = mapM_ drawAgent (sortBy (compare `on` agentMoney) as)
    where drawAgent agent = do
            let rect = agentRect agent
                format = SDL.surfaceGetPixelFormat screen
                (r, g, b) = agentRGB as agent
            color <- SDL.mapRGB format r g b
            SDL.fillRect screen (Just rect) color


agentLoop :: Int -> [Agent] -> IO ()
agentLoop 0 _ = do  -- If you've been at this 5 minutes, start over
  agents <- seedAgents population
  agentLoop 1 agents
agentLoop days as = SDL.pollEvent >>= handleEvent
    where
      handleEvent SDL.Quit = do
         putStrLn $ (show days) ++ " Days Elapsed:\n"
         mapM_ printInfo (sortBy (compare `on` agentId) as)
      handleEvent _ = do
        time <- SDL.getTicks
        if time `mod` 1000 == 0
        then do
          newAgents <- nextGen as
          let leftAlive = filter agentAlive as
          if length leftAlive > 1
          then do
            let clearF = clearSurface (0x40, 0x80, 0xC0)
                drawF = drawAgents as
            -- Ain't Monads neat?
            mapM (SDL.getVideoSurface >>=) [clearF, drawF, SDL.flip]
            agentLoop (days + 1) newAgents
          else do
            let clearF = clearSurface (0xC0, 0xC0, 0x40)
                drawF = drawAgents leftAlive
            mapM (SDL.getVideoSurface >>=) [clearF, drawF, SDL.flip]
            putStrLn $ (show days) ++ " Days Elapsed:\n"
            putStrLn "Last man standing:"
            printInfo $ head leftAlive
            SDL.delay 5000
            agentLoop 0 []
        else agentLoop days as


main :: IO ()
main = SDL.withInit [SDL.InitEverything] $ do
         vs <- SDL.setVideoMode scrW scrH 32 []
         SDL.setCaption "Moneygrab" "Monadgrab"
         clearSurface (0x40, 0x80, 0xC0) vs >> SDL.flip vs
         agentLoop 0 [] 
