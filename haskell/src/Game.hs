import qualified Graphics.UI.SDL as SDL
import Graphics.Rendering.OpenGL
    
import Foreign
import Foreign.C.Types
import Foreign.C.String
    
import Agent
    
import Data.List
import Data.Function
import qualified System.Random as Rand

type RGB = Color3 GLfloat

scrW :: Num a => a
scrH :: Num a => a
       
scrW = 600
scrH = 600


-- That got real simple, eh?
clearSurface :: RGB -> IO ()
clearSurface (Color3 r g b) = do
  clearColor $= (Color4 r g b 1)
  clear [ColorBuffer]


-- A night that grows longer and darker as more people die
colorLoop :: Float -> Float -> Float -> IO ()
colorLoop dead cnt freq = do
  let cos' x = (2 + cos x) / 4
      b = CFloat $ dead * cos' (cnt * freq)
  clearSurface $ Color3 (b / 2) (2 * b / 3) b


-- Determines the color of an agent based on its status in society
agentRGB :: [Agent] -> Agent -> RGB
agentRGB as agent =
    if agentAlive agent
    then let hshade = shade / 3
             shade = CFloat $ 0.25 + percent / 2
                 where percent = (agentMoney agent - lm) / (mm - lm)
                       (lm, mm) = foldl mnmx (hm, hm) money
                       mnmx = (\(n, m) x -> (min x n, max x m))
                       money = map agentMoney as
                       hm = head money
         in if agent `elem` (concat $ findOverlaps as)
            then Color3 hshade shade hshade -- Green overlaps
            else if null $ agentsOnMind agent
                 then Color3 shade shade shade -- Gray everyone else
                 else Color3 shade hshade hshade -- Red hunters
    else Color3 0 0 0 -- Black dead


overlay :: RGB -> RGB -> RGB
overlay (Color3 r g b) (Color3 r' g' b') =
    let o c1 c2 = if c1 < 0.5
                  then 2 * c1 * c2
                  else 1 - (2 * (1 - c1) * (1 - c2))
    in Color3 (o r' r) (o g' g) (o b' b)


renderAgents :: SDL.Window -> [Agent] -> IO ()
renderAgents window agents = do
  renderPrimitive Quads $ agentDisplay agents
  flush >> SDL.glSwapWindow window
    where agentDisplay [] = return ()
          agentDisplay (a:as) = renderAgent >> agentDisplay as
              -- Ain't Monads neat?
              where renderAgent = do
                               setColor
                               setCoord x0 y0
                               setCoord x1 y0
                               setCoord x1 y1
                               setCoord x0 y1
                    setColor = color $ agentRGB agents a 
                    setCoord x y = vertex $ Vertex3 x y (0 :: GLfloat)
                    x0 = (2 * (fromIntegral $ agentX a) / width) - 1
                    y0 = (2 * (fromIntegral $ agentY a) / height) - 1
                    x1 = (2 * (fromIntegral $ 1 + agentX a) / width) - 1
                    y1 = (2 * (fromIntegral $ 1 + agentY a) / height) - 1


agentLoop :: SDL.Window -> Int -> [Agent] -> IO ()
agentLoop win _ [] = do
  clearSurface $ Color3 0.25 0.50 0.75
  agents <- seedAgents population
  renderAgents win agents
  agentLoop win 1 agents
agentLoop win days agents = SDL.quitRequested >>= doesItHalt
    where doesItHalt no
              | no = halt
              | otherwise = do
            time <- SDL.getTicks
            if time `mod` 500 == 0
            then do
              gen <- Rand.newStdGen
              let alive = (filter agentAlive agents) 
                  numAlive = genericLength alive
              if numAlive > 1
              then do
                colorLoop (numAlive / population) (fromIntegral days) (recip 16)
                renderAgents win agents
                agentLoop win (days + 1) $ nextGen gen agents
              else do
                putStrLn $ (show days) ++ " Days Elapsed"
                putStrLn "Last man standing:"
                printInfo $ head alive
                agentLoop win 0 []
            else agentLoop win days agents
          halt = do
            mapM_ printInfo (sortBy (compare `on` agentId) agents)
            putStrLn $ (show days) ++ " Days Elapsed"

             
startRendering :: IO (SDL.Window, SDL.Renderer)
startRendering =
    alloca $ \wptr ->
        alloca $ \rptr -> do
          let flag = SDL.windowFlagShown .|. SDL.windowFlagOpenGL
          rc <- SDL.createWindowAndRenderer scrW scrH flag wptr rptr
          if rc == -1
          then fail "ERROR: Call to SDL.createWindowAndRenderer Failed"
          else do
            w <- peek wptr
            r <- peek rptr
            return (w, r)
 

main :: IO ()
main = do
  let flag = SDL.windowFlagShown .|. SDL.windowFlagOpenGL
      pos = SDL.windowPosUndefined
  SDL.init SDL.initFlagVideo
  w <- SDL.createWindow nullPtr pos pos scrW scrH flag
  withCString "Monadgrab" $ SDL.setWindowTitle w
  SDL.glCreateContext w
  agentLoop w 0 []
  SDL.destroyWindow w >> SDL.quit
