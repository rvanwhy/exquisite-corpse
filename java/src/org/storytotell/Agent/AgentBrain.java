package org.storytotell.Agent;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Outlines the general structure and workings of 
 * an Agent Brain
 * @author Randall R. Van Why
 *
 */
public abstract class AgentBrain {
	
	protected ArrayList<String> vocabulary;
	protected HashMap<String, Double> wordAssociationBank;
	protected enum Opinion {NEUTRAL, POSITIVE, NEGATIVE};
	
	public abstract double parse(String s);
	/**
	 * Associates a certain view with a word
	 * @param word, the word to associate
	 * @param v, the view
	 */
	protected void associateWord(String word, Opinion v) {
		double associationValue = 0;
		
		switch (v) {
		case NEUTRAL:
			associationValue = 0;
			break;
		case POSITIVE:
			associationValue = 1;
			break;
		case NEGATIVE:
			associationValue = -1;
			break;
		}
		
		this.vocabulary.add(word);
		this.wordAssociationBank.put(word, associationValue);	
	}
	
}
