package org.storytotell.Agent;

public enum BrainType {
	LEFT, RIGHT, CENTER;
}
