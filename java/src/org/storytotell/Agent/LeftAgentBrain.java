package org.storytotell.Agent;

/**
 * A left leaning agent brain. This brain attempts to match
 * word associations with stereotypical left leaning opinions.
 * @author Randall R. Van Why
 *
 */
public class LeftAgentBrain extends SimpleAgentBrain {
	private SimpleAgentBrain brain;
	
	public LeftAgentBrain(SimpleAgentBrain oldBrain) {
		brain = oldBrain;
		
		brain.associateWord("government", Opinion.NEGATIVE);
		brain.associateWord("rich_people", Opinion.NEGATIVE);
		brain.associateWord("progressive_tax", Opinion.POSITIVE);
		brain.associateWord("kill", Opinion.NEGATIVE);
	}
	
	public double parse(String s) {
		return brain.parse(s);
	}
}
