package org.storytotell.Agent;

import java.awt.Point;
import java.util.Random;

/**
 * Encapsulates the agent's state for cleaner agent code.
 * @author Randy Van Why
 */
public class AgentState {
	private static final int K = 32;
    private boolean alive;
    private long money;
    private double hfactor;
    private int elo;
    private long win, loss;
    private Point position;
    
    /**
     * Instantiate new agent state with specified parameters
     * @param money, the amount of money the agent has
     * @param position, the agent's starting position
     */
    public AgentState(long money, Point position) {
    	this.money = money;
        this.alive = true;
        this.elo = 1200;
        this.hfactor = 1.0;
        this.position = position;
    }
    
    public boolean overlaps(Agent other) {
    	//Simply check x and y for overlap
        return (position.getX() == other.getPosition().getX()) && (position.getY() == other.getPosition().getY());
    }
    
	public boolean isAlive() {
		return alive;
	}
	
	public void setAlive(boolean alive) {
		this.alive = alive;
	}
	
	public long getMoney() {
		return money;
	}
	
	public void setMoney(long money) {
		this.money = money;
	}
	
	public double getHfactor() {
		return hfactor;
	}
	
	/*
	 * TODO: Calculate hfactor according to some rule
	 */
	public void setHfactor(double hfactor) {
		this.hfactor = hfactor;
	}
	
	public int getElo() {
		return elo;
	}
	
	/**
	 * Adjusts the elo according to whether or not the
	 * agent won
	 * @param winner, whether or not the agent won
	 * @param win2, the amount to adjust the elo by
	 */
	public void adjustElo(double expected) {
		//Based on wikipedia article for Elo and its adjustments
        elo += K * ((win / (win + loss)) - expected);
	}
	
    public double adjustedElo() {
        return elo * hfactor;
    }
    
	public long getWin() {
		return win;
	}
	
	public void setWin(long win) {
		this.win = win;
	}
	
	public long getLoss() {
		return loss;
	}
	
	public void setLoss(long loss) {
		this.loss = loss;
	}
	
	public Point getPosition() {
		return position;
	}
	
	public void setPosition(int x, int y) {
		this.position.setLocation(x, y);
	}
	
    public void transferMoney(double amount, AgentState recipient) {
        money -= amount;
        recipient.addMoney(amount);
    }
    
    public void addMoney(double amount) {
    	money += amount;
    }
    
    public void youLost() {
        loss += 1;
    }
    
    public void youWon() {
        win += 1;
    }
    
    public void tryToPlayGameWith(Agent other) {
        if (overlaps(other)) grabMoney(other.getState());
    }
    
    public void grabMoney(AgentState other) {
        boolean weWon;
        double chanceOfUsWinning, chanceOfThemWinning;
        
        long ourMoney = this.money;
        long theirMoney = other.getMoney();
        long moneyAtStake = ((ourMoney > theirMoney) ? theirMoney : ourMoney) / 2;

        chanceOfUsWinning = calculateWinProbability(CalculationMethod.ELO, other);
        chanceOfThemWinning = other.calculateWinProbability(CalculationMethod.ELO, this);
        		
        weWon = determine_winner(chanceOfUsWinning);
        AgentState winner = weWon ? this  : other;
        AgentState loser  = weWon ? other : this;
        
        loser .transferMoney(moneyAtStake, winner);
        loser .youLost();
        winner.youWon();
        
        this.adjustElo(chanceOfUsWinning);
        other.adjustElo(chanceOfThemWinning);
    }
    
    /**
     * Calculate the chance of this agent winning against the other agent
     * @param m, the method of elo calculation
     * @param other, the other agent's state
     * @return the probability of this agent winning
     */
    private double calculateWinProbability(CalculationMethod m, AgentState other) {
        //Based on the wikipedia article for elo
        return 1 / (1 + Math.pow(10, ((m.getValue(other) - m.getValue(this)) / 400.0)));
    }
    
    private boolean determine_winner(double probability) {
        Random random = new Random();
        return random.nextDouble() < probability;
    }
    
    private enum CalculationMethod {
        ELO, ADJUSTED_ELO;

        public double getValue(AgentState a) {
            switch (this) {
                case ELO:          return a.getElo();
                case ADJUSTED_ELO: return a.adjustedElo();
            }

            return 0;
        }
    }
}
