package org.storytotell.Agent;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * This outlines the simple agent brain which will use
 * word associations to make decisions about messages
 * coming through the telescreen. Since this agent's brain
 * is simple, all word associations are neutral and thus do 
 * not affect decision making.
 * @author Randall R. Van Why
 *
 */
public class SimpleAgentBrain extends AgentBrain{

	public SimpleAgentBrain() {
		this.vocabulary = new ArrayList<String>();
		this.wordAssociationBank = new HashMap<String, Double>();
	}
	
	/**
	 * Parses a string and adds up associations to generate 
	 * an opinion about what was said.
	 * @return the opinion
	 * @see org.storytotell.Agent.AgentBrain#parse(java.lang.String)
	 */
	public double parse(String s) {
		double reaction = 0.0;
		
		for (String word : s.split(" ")) {
			if (vocabulary.contains(word)) {
				reaction += wordAssociationBank.get(word);
			}
		}
		
		return reaction;
	}
}
