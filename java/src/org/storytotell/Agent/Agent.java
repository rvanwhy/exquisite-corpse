package org.storytotell.Agent;

import java.awt.Point;
import java.util.Random;

import org.storytotell.Government.CivilianTelescreenController;
import org.storytotell.Government.GovernmentTelescreenController;
import org.storytotell.Government.TelescreenController;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

/**
 *
 * @author fusion
 */
public class Agent {
    private final long id;
    private boolean hasGovernmentBadge;
    private TelescreenController telescreen;
    private AgentState state;
    private AgentBrain brain;

    public Agent(final long id, long money,int x, int y) {
        this.id = id;
        this.state = new AgentState(money, new Point(x,y));
        this.telescreen = new CivilianTelescreenController(); /*wear it proudly*/
        this.hasGovernmentBadge = false;
        this.brain = new SimpleAgentBrain();
    }
    
	public long getId() {
		return id;
	}

    public AgentState getState() {
    	return state;
    }
    
    public double getMoney() {
        return this.state.getMoney();
    }
    
    public void setPosition(int x, int y) {
        this.state.setPosition(x, y);
    }
    
    public Point getPosition() {
        return state.getPosition();
    }
    
    public void listenToTelescreen() {
    	if (hasGovernmentBadge) {
    		return;
    	}
    	
    	brain.parse(telescreen.emitMessage());
    	
    }
    
    public void tryToPlayGameWith(Agent other) {
        state.tryToPlayGameWith(other);
    }
    
    public void upgradeToGovernmentAgent() {
    	this.telescreen = new GovernmentTelescreenController();
    	this.hasGovernmentBadge = true;
    }

    
}
