package org.storytotell.moneygrab;

 
import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.TimelineBuilder;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.stage.Stage;
import javafx.util.Duration;


/**
 *
 * @author fusion
 */
public class SimulationWindow extends Application {
    private Simulation sim;
    
    private Canvas canvas;
    
    final Duration oneFrameAmt = Duration.millis(500);
    final KeyFrame oneFrame = new KeyFrame(oneFrameAmt,
            new javafx.event.EventHandler<ActionEvent>() {

                public void handle(javafx.event.ActionEvent event) {
                    step();
                    draw();
                }
            }); // oneFrame

    public static void main(String[] args) {
        launch(args);
    }

    private void step() {
        for (int i = 0; i < 500; i++)
            sim.step();
    }
    
    private void draw() {
        GraphicsContext gc = canvas.getGraphicsContext2D();
        gc.clearRect(0, 0, 1000, 1000);
        sim.drawOn(gc);
    }
    
    @SuppressWarnings("deprecation")
	@Override
    public void start(Stage primaryStage) {
        sim = new Simulation(32, 20, 1000000, 50);
        
        primaryStage.setTitle("Simulation");
        Group root = new Group();
        canvas = new Canvas(300, 250);
        
        root.getChildren().add(canvas);
        
        // sets the game world's game loop (Timeline)
        TimelineBuilder.create()
                .cycleCount(Animation.INDEFINITE)
                .keyFrames(oneFrame)
                .build()
                .play();
        
        primaryStage.setScene(new Scene(root));
        primaryStage.show();
    }
}