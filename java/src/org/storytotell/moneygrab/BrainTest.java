package org.storytotell.moneygrab;

import java.lang.Override;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

public class BrainTest {
    /**
     * The possible ways of feeling about something.
     */
    public enum Disposition {
        GOOD, BAD, NEUTRAL;
    }

    /**
     * The possible ways of analyzing a statement.
     */
    public enum MetaDisposition {
        AGREE, DISAGREE, NO_OPINION;
    }

    /**
     * Things that have emotional reactions.
     */
    public interface Emotional {
        // I'd feel better about having an interface for SimpleStatement
        // but I can't think of an appropriate abstraction yet.
        public MetaDisposition reactionTo(SimpleStatement statement);
    }

    /**
     * Exceedingly fragile parser. All it can handle is "X is/are good/bad".
     *
     * @param sentence
     */
    public static SimpleStatement parse(String sentence) {
        String[]     result = sentence.split(" ");
        String      subject = result[0];
        Disposition opinion = Disposition.valueOf(result[2]);

        return new SimpleStatement(subject, opinion);
    }

    /**
     * A simple statement: SUBJECT is GOOD/BAD.
     */
    public static class SimpleStatement {
        private final String subject;
        private final Disposition disposition;

        public SimpleStatement(String subject, Disposition disposition) {
            this.subject = subject;
            this.disposition = disposition;
        }

        /**
         * Compare this statement to another and tell me whether we agree.
         *
         * @param other
         * @return
         */
        public MetaDisposition dispositionTowards(SimpleStatement other) {
           if (equals(other))
               return MetaDisposition.AGREE;
           else if (subject.equals(other.subject) && !disposition.equals(other.disposition))
               return MetaDisposition.DISAGREE;
           else
               return MetaDisposition.NO_OPINION;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            SimpleStatement that = (SimpleStatement) o;

            if (disposition != that.disposition) return false;
            if (!subject.equals(that.subject)) return false;

            return true;
        }

        @Override
        public int hashCode() {
            int result = subject.hashCode();
            result = 31 * result + disposition.hashCode();
            return result;
        }
    }

    public static class Brain implements Emotional {
        private final List<String> subjectsLiked    = new ArrayList<>();
        private final List<String> subjectsDisliked = new ArrayList<>();

        public Brain(String[] liked, String[] disliked) {
            this(Arrays.asList(liked), Arrays.asList(disliked));
        }

        public Brain(Collection<String> liked, Collection<String> disliked) {
            subjectsLiked.addAll(liked);
            subjectsDisliked.addAll(disliked);
        }

        @Override
        public MetaDisposition reactionTo(SimpleStatement statement) {
            if (subjectsLiked.contains(statement.subject))
                return new SimpleStatement(statement.subject, Disposition.GOOD).dispositionTowards(statement);
            else if (subjectsDisliked.contains(statement.subject))
                return new SimpleStatement(statement.subject, Disposition.BAD).dispositionTowards(statement);
            else
                return MetaDisposition.NO_OPINION;
        }

        // convenience method for testing
        public MetaDisposition reactionTo(String statement) {
            return reactionTo(parse(statement));
        }
    }

    public static void main(String[] args) {
        // let's make a test liberal and conservative brain
        Brain liberalBrain      = new Brain(new String[]{"NPR", "Priuses", "college"}, new String[]{"guns", "oil", "steak"});
        Brain conservativeBrain = new Brain(new String[]{"guns", "trucks", "God"},     new String[]{"Priuses", "cities", "Obama"});

        String[] subjects = new String[]{"NPR is", "Priuses are", "college is", "guns are", "oil is", "steak is", "trucks are", "God is", "cities are", "Obama is"};

        for (String subject : subjects) {
            for (Disposition d : Disposition.values()) {
                String sentence = subject + " " + d.toString();
                System.out.printf("liberalBrain:      '%s' -> %s\n", sentence, liberalBrain     .reactionTo(sentence));
                System.out.printf("conservativeBrain: '%s' -> %s\n", sentence, conservativeBrain.reactionTo(sentence));
            }
        }
    }
}