package org.storytotell.moneygrab;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.storytotell.Agent.Agent;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

/**
 *
 * @author fusion
 */
public class Simulation {
    private final List<Agent> agents;
    private final int PIXEL_SIZE;
    private final Random random;
    private final int  maxWidth, maxHeight;
    private final long totalMoney;  
    private long day;
    
    /**
     * Create a new simulation with {@code agentCount} agents.
     * @param maxWidth The maximum height of the sim
     * @param maxHeight The maximum width of the sim
     * @param totalMoney The total money available in the sim
     * @param agentCount The number of agents for this sim
     */
    public Simulation(int maxWidth, int maxHeight, long totalMoney, int agentCount) {
        this.agents = new ArrayList<Agent>(agentCount);
        this.random = new Random();
        
        this.maxWidth   = maxWidth;
        this.maxHeight  = maxHeight;
        this.totalMoney = totalMoney;
        
        for (int i = 0; i < agentCount; i++)
            agents.add(new Agent(i, totalMoney / agentCount, random.nextInt(maxWidth), random.nextInt(maxHeight)));
        
        this.PIXEL_SIZE = 10;

    }
    
    /**
     * Step the simulation forward by one loop iteration.
     */
    public void step() {
        day++;
        moveAgents();
        performExchanges();
    }
    
    /**
     * Move each of the agents one step randomly.
     */
    private void moveAgents() {
        for (Agent a : agents) {
            int randomX = ((random.nextInt(3) - 1) + (int) a.getPosition().getX() + maxWidth) % maxWidth;
            int randomY = (maxHeight + (int) a.getPosition().getY() + (random.nextInt(3) - 1)) % maxHeight;
            a.setPosition(randomX, randomY);         
        }
    }
    
    /**
     * Perform the money swap for agents that overlap.
     */
    private void performExchanges() {
        for (Agent left : agents)
            for (Agent right : agents.subList(agents.indexOf(left)+1, agents.size())) 
                left.tryToPlayGameWith(right);
    }
    
    /**
     * Draw the agents.
     * @param gc the graphics context which the agent is drawn on
     */
    public void drawOn(GraphicsContext gc) {
        for (Agent agent : agents) {
            gc.setFill(determineAgentColor(agent));
            gc.fillRect(agent.getPosition().getX() * PIXEL_SIZE, agent.getPosition().getY() * PIXEL_SIZE, PIXEL_SIZE, PIXEL_SIZE);
        }
    }
    
    /**
     * Color is calculated via percentage of agent's money to the total money
     * available within the simulation.
     * @param a the agent
     * @return the color of the agent
     */
    public Color determineAgentColor(Agent a) {
        int color = (int) ((a.getMoney() / totalMoney) * 255);
        return Color.rgb(color, color, color);
    }

    public int  getMaxHeight()  { return maxHeight;  }
    public int  getMaxWidth()   { return maxWidth;   }
    public long getTotalMoney() { return totalMoney; }
}
