package org.storytotell.Government;

/**
 * The outline for any telescreen controller derivative. Any
 * extention should allow for weak or strong interface with
 * the telescreen.
 * @author Randy
 */
public abstract class TelescreenController {
    private static String message;
    
    protected class Telescreen {
        private boolean on;

        public Telescreen() {
            on = true;
        }
    
        public String emitMessage() {
            String msg = "";
        
            if (this.isOn()) {
                msg = TelescreenController.message;
            }
        
            return msg;
        } 
    
        public void setMessage(String message) {
            TelescreenController.message = message;
        }
    
        public boolean isOn() {
            return on;
        }
    
        public void turnOff() {
            this.on = false;
        }
        
        public void turnOn() {
            this.on = true;
        }
    }
    
    public abstract String emitMessage();
    public abstract void turnOn();
}
