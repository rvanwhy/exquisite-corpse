package org.storytotell.Government;

import org.storytotell.Government.TelescreenController.Telescreen;

/**
 * Civilian telescreen unit. Doesn't allow you to do much but turn it on and
 * listen to its message.
 * @author Randy
 */
public class CivilianTelescreenController extends TelescreenController {
    private final Telescreen telescreen;
    
    public CivilianTelescreenController() {
        this.telescreen = new Telescreen();
    }
    
    public String emitMessage() {
        return this.telescreen.emitMessage();
    }
    
    public void turnOn() {
        this.telescreen.turnOn();
    }
}
