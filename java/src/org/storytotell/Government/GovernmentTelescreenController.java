package org.storytotell.Government;

import org.storytotell.Government.TelescreenController.Telescreen;

/**
 * The upgraded version of the CivilianTelescreenController.
 * Allows one to turn off the telescreen as well as change the message
 * that everyone sees.
 * @author Randy
 */
public class GovernmentTelescreenController extends TelescreenController {
    private final Telescreen telescreen;
    
    public GovernmentTelescreenController() {
        this.telescreen = new Telescreen();
    }
    
    public void changeMessage(String message) {
        this.telescreen.setMessage(message);
    }
        
    public String emitMessage() {
        return this.telescreen.emitMessage();
    }
    
    public void turnOff() {
        this.telescreen.turnOff();
    }
    
    public void turnOn() {
        this.telescreen.turnOn();
    }
}
